﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2017 v5.4.138
	 Created on:   	5/3/2017 12:20 PM
	 Created by:   	 Ben Claussen
	 Organization: 	NEOnet
	 Filename:     	Searches.ps1
	===========================================================================
	.DESCRIPTION
		Functions related to Searches
#>

function Get-CherwellSearchItems {
<#
	.SYNOPSIS
		Operation that returns a tree of saved queries, including scope, search name, IDs, and location within the tree
	
	.DESCRIPTION
		A detailed description of the Get-CherwellSearchItems function.
	
	.PARAMETER AssociationId
		Use to filter results by Business Object association ID. You can obtain the business object IDs through the
		Get-CherwellBusinessObject* cmdlets
	
	.PARAMETER Scope
		Use to filter results by scope name or ID. Defaults to Global
	
	.PARAMETER ScopeOwnerId
		Use to filter results by scope owner ID. Must be used with AssociationId and Scope.
	
	.PARAMETER FolderId
		Use to filter results by Search Group folder ID. Must be used with AssociationId, Scope, and ScopeOwner
	
	.PARAMETER IncludeLinks
		Switch to include hyperlinks in results. Default is false.
	
	.EXAMPLE
		PS C:\> Get-CherwellSearchItems
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(ValueFromPipelineByPropertyName = $true)]
		[Alias('busObId')]
		[string]$AssociationId,
		
		[ValidateSet('Global', 'User', 'Team', 'Role', 'Persona', IgnoreCase = $true)]
		[string]$Scope = 'Global',
		
		[string]$ScopeOwnerId,
		
		[string]$FolderId,
		
		[switch]$IncludeLinks
	)
	
	$Segments = [System.Collections.ArrayList]@('getsearchitems')
	
	if ($AssociationId) {
		if (-not (VerifyIDLength -Id $AssociationId)) {
			throw "Association Id is not a valid length."
		}
		
		$null = $Segments.AddRange(@('association', $AssociationId))
		
		if ($Scope) {
			$null = $Segments.AddRange(@('scope', $scope))
			
			if ($ScopeOwnerId) {
				if (-not (VerifyIDLength -Id $ScopeOwnerId)) {
					throw "ScopeOwnerId is not a valid length"
				}
				
				$null = $Segments.AddRange(@('scopeowner', $ScopeOwnerId))
				
				if ($FolderId) {
					if (-not (VerifyIDLength -Id $FolderId)) {
						throw "FolderId is not a valid length"
					}
					
					$null = $Segments.AddRange(@('folder', $FolderId))
				}
			}
		}
	}
	
	if ($IncludeLinks) {
		$uriParameters = @{
			"Links" = $IncludeLinks
		}
	}
	
	$uri = BuildNewURI -Segments $Segments -Parameters $uriParameters
	
	$searchItems = InvokeCherwellRequest -URI $uri
	
	if ($searchItems) {
		$searchItems.supportedAssociations = $searchItems.supportedAssociations | Sort-Object -Property 'busObName' -Unique
		$searchItems
	}
}

function Get-CherwellSearchResultsAdHoc {
<#
	.SYNOPSIS
		Execute an adhoc search
	
	.DESCRIPTION
		Operation that runs an ad-hoc Business Object search. To execute a search with Prompts, the PromptId and Value are required in the Prompt request object
	
	.PARAMETER busObId
		The Business Object ID to execute the search against
	
	.PARAMETER Fields
		The RecIds of the fields to return
	
	.PARAMETER IncludeAllFields
		Include all fields in the returned data
	
	.PARAMETER PageSize
		Size of the return result. Defaults to maximum
	
	.PARAMETER PageNumber
		A description of the PageNumber parameter.
	
	.PARAMETER Timeout
		Time in seconds for which the API call will time out and return an error.
	
	.PARAMETER Filters
		A collection of FilterInfo data structures. A FilterInfo contains the full field ID, operator and value.
		You can specify more than one filter. If you add multiple filters for the same field ID, the result
		is an OR operation between those fields. If the field IDs are different, the result is an AND operation
		between those fields
		
		Valid values are
	
	.PARAMETER IncludeSchema
		Include the schema in the data
	
	.EXAMPLE
		PS C:\> Get-CherwellSearchResults
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipeline = $true,
				   ValueFromPipelineByPropertyName = $true)]
		[Alias('BusinessObjectID')]
		[string[]]$busObId,
		
		[string[]]$Fields,
		
		[switch]$IncludeAllFields,
		
		[uint16]$PageSize = [uint16]::MaxValue,
		
		[uint32]$PageNumber,
		
		[int32]$Timeout,
		
		[hashtable[]]$Filters,
		
		[switch]$IncludeSchema
	)
	
	BEGIN {
		$uri = BuildNewURI -Segments 'getsearchresults'
		$body = @{
			'busObId' = $null
			"pageSize" = $PageSize
		}
		
		if ($PageSize) {
			$body.Add('pageNumber', $PageNumber)
		}
		
		if ($IncludeAllFields) {
			$body.Add('includeAllFields', $true)
		} elseif ($Fields) {
			$body.Add('fields', [System.Collections.ArrayList]::new())
			foreach ($field in $Fields) {
				$null = $body.Fields.Add($field)
			}
		}
		
		if ($Filters) {
			$body.Add('filters', [System.Collections.ArrayList]::new())
			foreach ($filter in $filters) {
				$null = $body.filters.add($filter)
			}
		}
		
		if ($IncludeSchema) {
			$body.Add('includeSchema', $true)
		}
		
		if (-not $Timeout) {
			$Timeout = Get-CherwellAPITimeout
		}
	}
	
	PROCESS {
		foreach ($id in $busObId) {
			$body.busObId = $id
			
			try {
				InvokeCherwellRequest -URI $uri -Body $body -Method POST -Timeout $Timeout
			} catch {
				if ($_.ErrorDetails.Message -match "The Business Object is not configured for searching") {
					$eSplat = @{
						'Message' = "The Business Object $id is not configured for searching."
						'Category' = $_.CategoryInfo.Category
						'CategoryActivity' = $_.CategoryInfo.Activity
						'CategoryReason' = $_.CategoryInfo.Reason
						'TargetObject' = $_.TargetObject
						'RecommendedAction' = "Configure the object $id for searching in Cherwell Administrator"
					}
					Write-Error @eSplat
				} else {
					Write-Error -ErrorRecord $_
				}
			}
		}
	}
	
	END {
		
	}
}

function Get-CherwellSearchResultsSavedSearch {
<#
	.SYNOPSIS
		Retrieves a saved search results by name
	
	.DESCRIPTION
		A detailed description of the Get-CherwellSearchResultsByName function.
	
	.PARAMETER Association
		Specify the Business Object association ID for the saved search
	
	.PARAMETER Scope
		Specify the scope name or ID for the saved search
	
	.PARAMETER ScopeOwner
		Specify the scope owner ID for the saved search. Use (None) when no scope owner exists
	
	.PARAMETER SearchName
		Specify the name of the saved search
	
	.PARAMETER SearchId
		Specify the internal ID for the saved search. Use the "SearchName" parameter if you do not have the internal ID
	
	.PARAMETER SearchTerm
		Specify search text filter the results. Example: Use "Service Request" to filter Incident results to include only service requests
	
	.PARAMETER PageNumber
		Specify the page number of the result set to return
	
	.PARAMETER PageSize
		Specify the number of rows to return per page
	
	.PARAMETER IncludeSchema
		Use to include the table schema of the saved search. If false, results contain the fieldid and field value without field information. Default is false
	
	.PARAMETER ResultsAsSimpleList
		Indicates if the results should be returned in a simple results list format or a table format. Default is a table format
	
	.PARAMETER Timeout
		A description of the Timeout parameter.
	
	.EXAMPLE
		PS C:\> Get-CherwellSearchResultsByName
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding(DefaultParameterSetName = 'SearchId')]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipelineByPropertyName = $true)]
		[string]$Association,
		
		[Parameter(Mandatory = $true,
				   ValueFromPipelineByPropertyName = $true)]
		[string]$Scope,
		
		[Parameter(Mandatory = $true,
				   ValueFromPipelineByPropertyName = $true)]
		[string]$ScopeOwner,
		
		[Parameter(ParameterSetName = 'SearchName',
				   Mandatory = $true,
				   ValueFromPipelineByPropertyName = $true)]
		[string]$SearchName,
		
		[Parameter(ParameterSetName = 'SearchId',
				   Mandatory = $true,
				   ValueFromPipelineByPropertyName = $true)]
		[string]$SearchId,
		
		[string]$SearchTerm,
		
		[uint32]$PageNumber,
		
		[uint32]$PageSize,
		
		[switch]$IncludeSchema,
		
		[switch]$ResultsAsSimpleList,
		
		[System.Nullable[int32]]$Timeout
	)
	
	BEGIN {
		$uriParameters = @{
		}
		
		if ($SearchTerm) {
			Write-Debug "Adding searchTerm parameter"
			$uriParameters.Add('searchTerm', $SearchTerm)
		}
		
		if ($PageNumber) {
			Write-Debug "Adding pageNumber parameter"
			$uriParameters.Add('pageNumber', $PageNumber)
		}
		
		if ($PageSize) {
			Write-Debug "Adding pageSize parameter"
			$uriParameters.Add('pageSize', $PageSize)
		}
		
		if ($IncludeSchema) {
			Write-Debug "Adding includeSchema parameter"
			$uriParameters.Add('includeSchema', 'true')
		}
		
		if ($ResultsAsSimpleList) {
			Write-Debug "Adding resultsAsSimpleList parameter"
			$uriParameters.Add('resultsAsSimpleList', 'true')
		}
		
		if ($Timeout -eq $null) {
			$Timeout = Get-CherwellAPITimeout
			Write-Verbose "Setting timeout to default: $Timeout"
		}
	}
	
	PROCESS {
		$Segments = [System.Collections.ArrayList]@('getsearchresults', 'association', $Association, 'scope', $Scope, 'scopeowner', $ScopeOwner)
		
		switch ($PSCmdlet.ParameterSetName) {
			'SearchId' {
				Write-Debug "Using parameter set $_"
				if (-not (VerifyIDLength -Id $SearchId)) {
					throw "ID is not a valid length"
				}
				$Segments.AddRange(@('searchid', $SearchId))
			}
			
			'SearchName' {
				Write-Debug "Using parameter set $_"
				$Segments.AddRange(@('searchname', $SearchName))
			}
		}

		$uri = BuildNewURI -Segments $Segments -Parameters $uriParameters
		
		InvokeCherwellRequest -URI $uri -Timeout $Timeout
	}
	
	END {
	}
}


function Convert-CherwellSearchResultsToArray {
	[CmdletBinding()]
	[OutputType([System.Collections.ArrayList])]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipelineByPropertyName = $true,
				   Position = 0)]
		[pscustomobject[]]$businessObjects
	)
	
	BEGIN {
		$list = [System.Collections.ArrayList]::new()
	}
	
	PROCESS {
		foreach ($obj in $businessObjects) {
			$listObject = @{}
			
			foreach ($field in $obj.fields) {
				$listObject[$field.Name] = $field.value
			}
			
			$null = $list.Add([pscustomobject]$listObject)
		}
	}
	
	END {
		,$list # Use comma to force System.Collections.ArrayList
	}
}

function New-CherwellSearchFilter {
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[string]$FieldId,
		
		[Parameter(Mandatory = $true,
				   Position = 1)]
		[ValidateSet('Equal', 'eq', 'GreaterThan', 'gt', 'LessThan', 'lt', 'Contains', 'StartsWith', IgnoreCase = $true)]
		[string]$Operator,
		
		[Parameter(Mandatory = $true,
				   Position = 2)]
		[string]$Value
	)
	
	function ConvertOperator {
		[CmdletBinding()]
		[OutputType([string])]
		param
		(
			[Parameter(Mandatory = $true,
					   Position = 0)]
			[string]$Operator
		)
		
		switch -wildcard ($Operator) {
			"eq*" {
				return 'eq'
			}
			
			{ $_ -in "gt", "greaterthan" } {
				return 'gt'
			}
			
			{ $_ -in "lt", "lessthan" } {
				return "lt"
			}
			
			"contains" {
				return $_
			}
			
			"startswith" {
				return $_
			}
			
			default {
				throw "Unable to translate operator $_"
			}
		}
	}
	
	return @{
		'fieldId' = $FieldId
		'operator' = (ConvertOperator -Operator $Operator)
		'value' = $Value
	}
}

function New-CherwellSearchSort {
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[string]$FieldId,
		
		[Parameter(Mandatory = $false,
				   Position = 1)]
		[ValidateSet('0', 'Descending', '1', 'Ascending', IgnoreCase = $true)]
		[string]$SortDirection = '0'
	)
	
	function ConvertSortDirection {
		[OutputType([byte])]
		param
		(
			[Parameter(Mandatory = $true,
					   Position = 0)]
			[string]$Direction
		)
		
		switch ($Direction) {
			{$_ -in "0", "Descending"} {
				return 0
			}
			
			{$_ -in "1", "Ascending"} {
				return 1
			}
			
			default {
				throw "Invalid sort direction provided"
			}
		}
	}
	
	return @{
		'fieldId' = $FieldId
		'sortDirection' = (ConvertSortDirection -Direction $SortDirection)
	}
}






