﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2017 v5.4.138
	 Created on:   	5/3/2017 12:34 PM
	 Created by:   	 Ben Claussen
	 Organization: 	NEOnet
	 Filename:     	BusinessObjects.ps1
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>

function Get-CherwellBusinessObjectSummaryById {
<#
	.SYNOPSIS
		Get a single Business Object's summary
	
	.DESCRIPTION
		Operation that returns a single Business Object summary by ID.
	
	.PARAMETER BusinessObjectId
		Specify a Business Object ID to get its summary
	
	.EXAMPLE
		PS C:\> Get-CherwellBusinessObjectSummaryById -BusinessObjectId 'Value1'
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[Alias('BusObId')]
		[string]$BusinessObjectId
	)
	
	$uri = BuildNewURI -Segments 'getbusinessobjectsummary', 'busobid', $BusinessObjectId
	
	return InvokeCherwellRequest -URI $uri
}

function Get-CherwellBusinessObjectSummaryByName {
<#
	.SYNOPSIS
		Get a particular Business Objecy by the name
	
	.DESCRIPTION
		Operation that returns a single Business Object summary by name
	
	.PARAMETER BusinessObjectName
		Specify a Business Object name to get its summary
	
	.EXAMPLE
		PS C:\> Get-BusinessObjectSummaryByName -BusinessObjectName 'Value1'
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[Alias('BusObName')]
		[string]$BusinessObjectName
	)
	
	$uri = BuildNewURI -Segments 'getbusinessobjectsummary', 'busobname', $BusinessObjectName
	
	return InvokeCherwellRequest -URI $uri
}

function Get-CherwellBusinessObjectSummariesByType {
<#
	.SYNOPSIS
		Get business object summaries by a particular type.
	
	.DESCRIPTION
		Operation that returns a list of Business Object summaries by type (Major, Supporting, Lookup, Groups, and All)..
	
	.PARAMETER Type
		Use to show:
		All - All objects (default)
		Major - Major objects only
		Supporting - Supporting objects only
		Lookup - Lookup objects only
		Groups - Groups only
	
	.PARAMETER Force
		Forces a refresh from the server instead of using the local cache
	
	.EXAMPLE
		PS C:\> Get-CherwellBusinessObjectSummariesByType
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	param
	(
		[ValidateSet('All', 'Major', 'Supporting', 'Lookup', 'Groups', IgnoreCase = $true)]
		[string]$Type = "All",
		
		[switch]$Force
	)
	
	$CachedSummaries = $script:CherwellConfig.CachedData.BusinessObjects.Summaries
	
	if ((-not $CachedSummaries) -or ($Force)) {
		Write-Verbose "Refreshing BusinessObjectSummaries from server and caching results"
		$uri = BuildNewURI -Segments 'getbusinessobjectsummaries', 'type', $Type
		
		$CachedSummaries = InvokeCherwellRequest -URI $uri
		
		$script:CherwellConfig.CachedData.BusinessObjects.Summaries = $CachedSummaries
	} else {
		Write-Verbose "Using cached BusinessObjectSummaries"
	}
	
	$CachedSummaries
}

function Find-CherwellBusinessObjectByDisplayName {
<#
	.SYNOPSIS
		Search Cherwell business objects to find one by display name
	
	.DESCRIPTION
		This will obtain (by default) all business object summaries from Cherwell and filter the
		results to only those matching the DisplayName parameter. You may specify different match types.
	
	.PARAMETER DisplayName
		The display name of the business object to find.
	
	.PARAMETER MatchType
		Different methods of matching the display name. For example, if you choose 'Equal',
		only those business objects where the display name EQUALS your provided display name
		parameter will be returned. By default, this uses regular expression (powershell -match operator)
	
	.PARAMETER Type
		Only search a particular type of business objects. Default is "All"
	
	.PARAMETER Force
		Forces a refresh of the summary information from the server instead of pulling from cached data
	
	.EXAMPLE
		PS C:\> Find-CherwellBusinessObjectByDisplayName -DisplayName 'Contact'
		
		This will return all Business Objects where the display name MATCHES "contact" by regular expression.
	
	.EXAMPLE
		PS C:\> Find-CherwellBusinessObjectByDisplayName -DisplayName 'Contact' -MatchType 'Equal'
		
		This will return all Business Objects where the display name EQUALS "Contact".
	
	.EXAMPLE
		PS C:\> Find-CherwellBusinessObjectByDisplayName -DisplayName 'Contact' -MatchType 'Equal' -Type 'Supporting'
		
		This will return all Business Objects where the display name EQUALS "Contact", and ONLY 'Supporting' objects.
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	[OutputType([pscustomobject[]])]
	param
	(
		[Parameter(Mandatory = $true)]
		[string]$DisplayName,
		
		[ValidateSet('Match', 'Like', 'NotLike', 'Equal', 'NotEqual', 'StartsWith', IgnoreCase = $true)]
		[string]$MatchType = 'Match',
		
		[ValidateSet('All', 'Major', 'Supporting', 'Lookup', 'Groups', IgnoreCase = $true)]
		[string]$Type = "All",
		
		[switch]$Force
	)
	
	# Firstly, get the business object summaries by type
	$busObjSummaries = Get-CherwellBusinessObjectSummariesByType -Type $Type -Force:$Force.ToBool()
	
	# Loop through each one and output those whose displayname matches $DisplayName
	foreach ($businessObject in $busObjSummaries) {
		# Is this a group?
		if ($businessObject.group -eq $true) {
			foreach ($subBusinessObject in $businessObject.groupSummaries) {
				#if ($subBusinessObject.displayName -match $DisplayName) {
				if ((CompareValue $subBusinessObject.displayName $MatchType $DisplayName)) {
					Write-Output $subBusinessObject
				}
			}
		} else {
			#if ($businessObject.displayName -match $DisplayName) {
			if ((CompareValue $businessObject.displayName $MatchType $DisplayName)) {
				Write-Output $businessObject
			}
		}
	}
}

function Get-CherwellBusinessObjectSchema {
<#
	.SYNOPSIS
		Returns the schema for a Business Object and, optionally, its related Business Objects
	
	.DESCRIPTION
		Returns the schema for a Business Object and, optionally, its related Business Objects
	
	.PARAMETER BusObId
		The object(s) returned from a Cherwell call to get a business object, or string(s).
	
	.PARAMETER IncludeRelationships
		Switch to include schemas for related Business Object
	
	.EXAMPLE
		PS C:\> Get-CherwellBusinessObjectSchema -BusObId $value1
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipeline = $true,
				   ValueFromPipelineByPropertyName = $true,
				   Position = 0)]
		[Alias('BusinessObjectId')]
		[object[]]$BusObId,
		
		[switch]$IncludeRelationships
	)
	
	Begin { }
	
	Process {
		foreach ($businessObject in $BusObId) {
			if ($IncludeRelationships) {
				$uriParameters = @{
					'includerelationships' = $true
				}
			}
			
			Write-Verbose "BusObId is $($businessObject.GetType().FullName)"
			
			if (($businessObject -is [pscustomobject]) -and ($businessObject.busObId)) {
				Write-Verbose 'BusObId property exists on BusObId'
				$id = $businessObject.busObId
			} else {
				Write-Verbose "BusObId property does not exist on BusObId"
				$id = $businessObject
			}
			
			if ((VerifyIDLength $id)) {
				$uri = BuildNewURI -Segments @('getbusinessobjectschema', 'busobid', $id) -Parameters $uriParameters
				
				InvokeCherwellRequest -URI $uri
			} else {
				Write-Error -Message "Id $id is not valid." -Category InvalidData -TargetObject $businessObject -RecommendedAction 'Check that the ID provided is correct'
			}
		}
	}
	
	End {
		
	}
}

function Find-CherwellFieldNameInSchemaByDisplayName {
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipelineByPropertyName = $true,
				   Position = 0)]
		[object[]]$FieldDefinitions,
		
		[Parameter(Position = 2)]
		[string]$DisplayName,
		
		[Parameter(Position = 1)]
		[ValidateSet('Match', 'Like', 'NotLike', 'Equal', 'NotEqual', 'StartsWith', IgnoreCase = $true)]
		[string]$MatchType = 'Match'
	)
	
	BEGIN {
		if ($PSCmdlet.MyInvocation.PipelineLength -gt 1) {
			Write-Verbose "In pipeline; Position $($PSCmdlet.MyInvocation.PipelinePosition); Length: $($PSCmdlet.MyInvocation.PipelineLength)"
			$InPipeline = $true
		} else {
			Write-Verbose "Not in pipeline"
			$InPipeline = $false
		}
	}
	
	PROCESS {
		foreach ($field in $FieldDefinitions) {
			if ((CompareValue $field.displayName $MatchType $DisplayName)) {
				Write-Output $field
			}
		}
	}
	
	END {
		
	}
}

function Get-CherwellBusinessObjectRecord {
<#
	.SYNOPSIS
		Get a business object record
	
	.DESCRIPTION
		Operation that returns a Business Object record that includes a list of fields and their record IDs, names, and set values
	
	.PARAMETER busObId
		A description of the busObId parameter.
	
	.PARAMETER busObRecId
		A description of the busObRecId parameter.
	
	.PARAMETER busObPublicId
		A description of the busObPublicId parameter.
	
	.EXAMPLE
		PS C:\> Get-CherwellBusinessObjectRecord -busObId '42characterObjectId' -busObRecId '42characterRecId'
		PS C:\> Get-CherwellBusinessObjectRecord -busObId '42characterObjectId' -busObPublicId 'publicId'
	
	.OUTPUTS
		pscustomobject
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding(DefaultParameterSetName = 'RecId')]
	[OutputType([pscustomobject], ParameterSetName = 'RecId')]
	[OutputType([pscustomobject], ParameterSetName = 'PublicId')]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true)]
		[string]$busObId,
		
		[Parameter(ParameterSetName = 'RecId',
				   Mandatory = $true)]
		[string]$busObRecId,
		
		[Parameter(ParameterSetName = 'PublicId',
				   Mandatory = $true)]
		[string]$busObPublicId
	)
	
	switch ($PsCmdlet.ParameterSetName) {
		'RecId' {
			$uri = BuildNewURI -Segments 'busObId', $busObId, 'busObRecId', $busObRecId
			break
		}
		'PublicId' {
			$uri = BuildNewURI -Segments 'busObId', $busObId, 'publicid', $busObPublicId
			break
		}
	}
	
	InvokeCherwellRequest -URI $uri
}


function Get-CherwellBusinessObjectRecordBatch {
<#
	.SYNOPSIS
		Obtain a batch of Business Object Records
	
	.DESCRIPTION
		Operation that returns a batch of Business Object records that includes a list of field record IDs, display names, and values for each record.
	
	.PARAMETER ReadRequests
		Specify an array of Business Object IDs, record IDs, or public IDs. Must be in format @{'busObId' = '<busobid>'}, etc...
	
	.PARAMETER StopOnError
		Flag to stop on error or continue on error. Defaults to continue on error
	
	.EXAMPLE
				PS C:\> Get-CherwellBusinessObjectRecordBatch -ReadRequests $value1
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[pscustomobject[]]$ReadRequests,
		
		[Parameter(Position = 1)]
		[switch]$StopOnError
	)
	
	$uri = BuildNewURI -Segments 'getbusinessobjectbatch'
	
	$body = @{
		'readRequests' = [System.Collections.ArrayList]::new($ReadRequests)
		'stopOnError' = $StopOnError.ToBool()
	}
	
	return InvokeCherwellRequest -URI $uri -Body $body -Method POST
}

function New-CherwellBusinessObjectRecordBatchReadRequest {
<#
	.SYNOPSIS
		Create a new ReadRequest for a BusinessObject Batch request
	
	.DESCRIPTION
		Creates the appropriate request object for a batch record request
	
	.PARAMETER BusObId
		The business object ID
	
	.PARAMETER BusObPublicId
		The business object public ID
	
	.PARAMETER BusObRecId
		The business object record ID
	
	.EXAMPLE
		PS C:\> New-CherwellBusinessObjectBatchReadRequest -BusObId 'Value1'
	
	.OUTPUTS
		pscustomobject
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding(DefaultParameterSetName = 'BusObId')]
	[OutputType([pscustomobject], ParameterSetName = 'BusObId')]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(ParameterSetName = 'BusObId',
				   Mandatory = $true)]
		[string]$BusObId,
		
		[Parameter(ParameterSetName = 'BusObPublicId',
				   Mandatory = $true)]
		[string]$BusObPublicId,
		
		[Parameter(Mandatory = $true)]
		[string]$BusObRecId
	)
	
	$object = @{
		'busObRecId' = $BusObRecId
	}
	
	switch ($PsCmdlet.ParameterSetName) {
		'BusObId' {
			$object.Add('busObId', $BusObId)
			break
		}
		'BusObPublicId' {
			$object.Add('busObPublicId', $BusObPublicId)
			break
		}
	}
	
	return [pscustomobject]$object
}

function New-CherwellBusinessObjectRecord {
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipelineByPropertyName = $true)]
		[string]$busObId,
		
		[Parameter(Mandatory = $true)]
		[pscustomobject[]]$fields
	)
	
	Begin {
		$uri = BuildNewURI -Segments 'savebusinessobject'
	}
	
	Process {
		$body = @{
			'busObId' = $businessObject
			'fields' = $fields
		}
		
		InvokeCherwellRequest -URI $uri -Body $body -Method POST
	}
	
	End {
		
	}
}

function Update-CherwellBusinessObjectRecord {
<#
	.SYNOPSIS
		Update an exising business object record
	
	.DESCRIPTION
		Operation that updates an existing Business Object. To update, specify record ID or public ID.
	
	.PARAMETER busObId
		A description of the busObId parameter.
	
	.PARAMETER busObRecId
		A description of the busObRecId parameter.
	
	.PARAMETER busObPublicId
		A description of the busObPublicId parameter.
	
	.PARAMETER fields
		A description of the fields parameter.
	
	.EXAMPLE
		PS C:\> Update-CherwellBusinessObjectRecord
	
	.OUTPUTS
		pscustomobject, pscustomobject
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipelineByPropertyName = $true)]
		[string]$busObId,
		
		[Parameter(ValueFromPipelineByPropertyName = $true)]
		[string]$busObRecId,
		
		[Parameter(ValueFromPipelineByPropertyName = $true)]
		[string]$busObPublicId,
		
		[Parameter(ValueFromPipelineByPropertyName = $true)]
		[pscustomobject[]]$Fields
	)
	
	BEGIN {
		$uri = BuildNewURI -Segments 'savebusinessobject'
	}
	
	PROCESS {
		$body = @{
			'busObId' = $busObId
			'fields' = $fields
		}
		
		if ($busObRecId) {
			Write-Verbose "Adding RecId for update"
			$body.Add('busObRecId', $busObRecId)
		} elseif ($busObPublicId) {
			Write-Verbose "Adding PublicId for update"
			$body.Add('busObPublicId', $busObPublicId)
		} else {
			Write-Error -Message "Did not find a RecId or PublicId on the object" -Category InvalidArgument -TargetObject $_
			continue
		}
		
		InvokeCherwellRequest -URI $uri -Body $body -Method POST
	}
	
	END {
		
	}
}

function Set-CherwellBusinessObjectRecordFieldValue {
	[CmdletBinding()]
	[OutputType([void])]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipeline = $true,
				   Position = 0)]
		[pscustomobject]$Field,
		
		[Parameter(Mandatory = $true,
				   Position = 1)]
		[string]$NewValue
	)
	
	Begin {
		
	}
	
	Process {
		# Check there is a value and dirty property
		if (-not ($Field.psobject.properties.name -contains 'value')) {
			throw "'Value' property missing from field object"
		} elseif (-not ($Field.psobject.properties.name -contains 'dirty')) {
			throw "'Dirty' property missing from field object"
		}
		
		if ($Field.value -ne $NewValue) {
			Write-Verbose "Updating value"
			$Field.value = $NewValue
			$Field.dirty = $true
		} else {
			Write-Verbose "Value not changed"
		}
	}
	
	End {
		
	}
}


function New-CherwellBusinessObjectRecordField {
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true)]
		[string]$displayName,
		
		[Parameter(Mandatory = $true)]
		[string]$fieldId,
		
		[Parameter(Mandatory = $true)]
		[string]$name,
		
		[Parameter(Mandatory = $true)]
		[string]$value
	)
	
	return @{
		'dirty' = $true
		'displayName' = $displayName
		'fieldId' = $fieldId
		'name' = $name
		'value' = $value
	}
}

function Get-CherwellBusinessObjectTemplate {
	[CmdletBinding(DefaultParameterSetName = 'RequiredFields')]
	[OutputType([pscustomobject], ParameterSetName = 'RequiredFields')]
	[OutputType([pscustomobject], ParameterSetName = 'SpecificFields')]
	[OutputType([pscustomobject], ParameterSetName = 'AllFields')]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipelineByPropertyName = $true)]
		[string]$busObId,
		
		[Parameter(ParameterSetName = 'SpecificFields',
				   Mandatory = $true)]
		[string[]]$FieldNames,
		
		[Parameter(ParameterSetName = 'AllFields')]
		[switch]$IncludeAllFields,
		
		[Parameter(ParameterSetName = 'RequiredFields')]
		[switch]$IncludeRequiredFields
	)
	
	$uri = BuildNewURI -Segments 'getbusinessobjecttemplate'
	
	$body = @{
		'busObId' = $busObId
	}
	
	switch ($PsCmdlet.ParameterSetName) {
		'SpecificFields' {
			$body.Add('fieldNames', [System.Collections.ArrayList]::new($FieldNames))
			break
		}
		'AllFields' {
			$body.Add('includeAll', $true)
			break
		}
		'RequiredFields' {
			$body.Add('includeRequired', $true)
			break
		}
	}
	
	InvokeCherwellRequest -URI $uri -Body $body -Method POST
}








