﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2017 v5.4.138
	 Created on:   	5/1/2017 11:43 AM
	 Created by:   	Ben Claussen
	 Organization: 	NEOnet
	 Filename:     	Helpers.ps1
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>

function CheckCherwellIsConnected {
	[CmdletBinding()]
	param ()
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	if ((-not $script:CherwellConfig) -or (-not $script:CherwellConfig.Connected)) {
		throw "Not connected to a Cherwell API! Please run 'Connect-CherwellAPI'"
	}
	
	return $true
}

function BuildNewURI {
<#
	.SYNOPSIS
		Create a new URI
	
	.DESCRIPTION
		Create a [System.UriBuilder] for easy URI manipulation.
	
	.PARAMETER Hostname
		Hostname of the API
	
	.PARAMETER Port
		Port number on which the web service is provided. Defaults to 443
	
	.PARAMETER Segments
		Array of strings for each segment in the URL path
	
	.PARAMETER Parameters
		Hashtable of query parameters to include
	
	.PARAMETER HTTPS
		Whether to use HTTPS or HTTP. Defaults to TRUE. You must explicitly disable HTTPS by setting this to FALSE. False is a bad idea.
	
	.PARAMETER TokenRequest
		Generate a token request.
	
	.PARAMETER V2
		Use version 2 of the API.
	
	.EXAMPLE
		PS C:\> BuildNewURI
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	[OutputType([System.UriBuilder])]
	param
	(
		[Parameter(Mandatory = $false)]
		[string]$Hostname,
		
		[Parameter(Mandatory = $false)]
		[uint16]$Port = 443,
		
		[Parameter(Mandatory = $false)]
		[string[]]$Segments,
		
		[Parameter(Mandatory = $false)]
		[AllowNull()]
		[hashtable]$Parameters,
		
		[Parameter(Mandatory = $false)]
		[boolean]$HTTPS = $true,
		
		[Parameter(Mandatory = $false)]
		[switch]$TokenRequest = $false,
		
		[switch]$V2
	)
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	if (-not $Hostname) {
		$Hostname = Get-CherwellHostname
	}
	
	if (-not $HTTPS) {
		Write-Warning "Using HTTP is insecure and credentials are passed in clear text. Please consider utilizing HTTPS."
	}
	
	# Begin a URI builder with HTTP/HTTPS and the provided hostname
	$uriBuilder = [System.UriBuilder]::new($(if ($HTTPS) {'https'} else {'http'}), $Hostname, $port)
	
	if ($TokenRequest) {
		# Requesting a token uses a different path from the regular API
		$uriBuilder.Path = "CherwellAPI/token"
		Write-Verbose "This is a token URI... using $($uriBuilder.Path)"
	} else {
		# Generate the path by trimming excess slashes and whitespace from the $segments[] and joining together
		$uriBuilder.Path = "CherwellAPI/api/{0}/{1}" -f $(if ($V2) {'v2'} else {'v1'}), ($Segments.ForEach({$_.trim('/').trim()}) -join '/')
		Write-Verbose "This is a normal URI... using $($uriBuilder.Path)"
	}
	
	if ($parameters) {
		Write-Debug "Adding parameters to URIBuilder"
		# Loop through the parameters and use the HttpUtility to create a Query string
		$URIParams = [System.Web.HttpUtility]::ParseQueryString([String]::Empty)
		
		foreach ($param in $Parameters.GetEnumerator()) {
			$URIParams[$param.Key] = $param.Value
			Write-Debug "Added $($param.Key):$($param.Value)"
		}
		
		$uriBuilder.Query = $URIParams.ToString()
	}
	
	# Return the entire UriBuilder object
	$uriBuilder
}

function AutoRefreshCherwellAccessToken {
	[CmdletBinding()]
	param ()
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	if ($script:CherwellConfig.Token.Expires -lt (Get-Date)) {
		Write-Verbose "The token has expired"
		
		if ($script:CherwellConfig.AutoRefreshToken -eq $true) {
			# We are set to automatically refresh
			Write-Warning "Automatically refreshing token"
			SetCherwellConfigToken -Token (Get-CherwellToken)
		} else {
			throw "Token expired at $($script:CherwellConfig.Token.Expires) and AutoRefresh is disabled"
		}
	} else {
		Write-Verbose "Token is still valid for $('{0:n2}' -f ($script:CherwellConfig.Token.Expires - (Get-Date)).TotalMinutes) minutes"
		return $true
	}
}

function InvokeCherwellRequest {
<#
	.SYNOPSIS
		Internal function. Sends an HTTP request to Cherwell
	
	.DESCRIPTION
		Generates a proper HTTP request including headers and body to send to a Cherwell server
	
	.PARAMETER URI
		A System.UriBuilder object which will generate the full URL including scheme, hostname, path, and parameters
	
	.PARAMETER Headers
		A System.Hashtable object which will be converted to the HTTP headers sent to the server
	
	.PARAMETER Body
		A System.Hashtable of parameters which will be sent as a JSON body to the server
	
	.PARAMETER Timeout
		The time in seconds for Invoke-RestMethod to give up without receiving a response from the server. The default is determined by Get-CherwellAPITimeout. A value of 0 will set it to a maximum timeout.
	
	.PARAMETER Method
		The HTTP method to use for the request. Defaults to GET
	
	.PARAMETER ContentType
		The content type header sent to the server. Defaults to 'application/json'
	
	.PARAMETER BypassAuthorizationHeader
		Skip the check for the Authorization Header. Generally only used for a token request
	
	.PARAMETER TokenRequest
		Determines whether this is a token request which requires a different base path.
	
	.EXAMPLE
		PS C:\> InvokeCherwellRequest -URI $value1
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding(SupportsShouldProcess = $true)]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true)]
		[System.UriBuilder]$URI,
		
		[Hashtable]$Headers = @{ },
		
		[hashtable]$Body = $null,
		
		[AllowNull()]
		[System.Nullable[int32]]$Timeout,
		
		[ValidateSet('GET', 'PATCH', 'PUT', 'POST', 'DELETE', IgnoreCase = $true)]
		[string]$Method = 'GET',
		
		[ValidateSet('application/json', 'application/x-www-form-urlencoded', IgnoreCase = $true)]
		[string]$ContentType = "application/json",
		
		[switch]$BypassAuthorizationHeader,
		
		[switch]$TokenRequest
	)
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	Write-Verbose "Building request invocation..."
	
	# Determine if we need to obtain the timeout value or set it to maximum
	if ($Timeout -eq $null) {
		$Timeout = Get-CherwellAPITimeout
		Write-Verbose "Timeout not provided.. Setting timeout to config variable default: $Timeout"
	} elseif ($Timeout -eq 0) {
		Write-Warning "Timeout value set to maximum!"
		$Timeout = [int32]::MaxValue
	}
	
	if ($TokenRequest) {
		Write-Verbose "Skipping authorization header check"
		$BypassAuthorizationHeader = $true
	} else {
		$null = AutoRefreshCherwellAccessToken -ErrorAction Stop
	}
	
	if (-not $BypassAuthorizationHeader) {
		# We are NOT bypassing the authorization header check
		if (-not $Headers.Authorization) {
			# Add the Authorization header if it does not exist
			Write-Verbose 'Adding authorization to headers'
			$null = $Headers.Add('Authorization', ("Bearer {0}" -f (GetCherwellConfigToken).AccessToken))
		}
	}
	
	$splat = @{
		'Method' = $Method
		'Uri' = $URI.Uri.AbsoluteUri # This property auto generates the scheme, hostname, path, and query
		'Headers' = $Headers
		'TimeoutSec' = $Timeout
		'ContentType' = $ContentType
		'ErrorAction' = 'Stop'
		'Verbose' = $VerbosePreference
	}
	
	if ($Body) {
		if ($ContentType -eq 'application/json') {
			Write-Verbose "Converting body to JSON"
			$null = $splat.Add('Body', ($Body | ConvertTo-Json -Compress))
			Write-Verbose "JSON Body: $($splat.Body)"
		} else {
			Write-Verbose "Using x-www-form-urlencoded body"
			$null = $splat.Add('Body', $Body)
		}
		
		# Pretty hashtable verbose output
		$columnWidth = $body.Keys.length | Sort-Object | Select-Object -Last 1
		Write-Verbose "Body hashtable:"
		$Body.GetEnumerator() | ForEach-Object {
			if ($_.Key -eq 'password') {
				Write-Verbose ("  {0,-$columnWidth} : {1}" -F $_.Key, '***HIDDEN***')
			} else {
				Write-Verbose ("  {0,-$columnWidth} : {1}" -F $_.Key, $_.Value)
			}
		}
	}
	
	if (($Method -eq 'POST') -and $Body) {
		Write-Verbose "A -1-byte payload is expected in the verbose output below!"
	}
	
	$timestamp = (Get-Date)
	$results = Invoke-RestMethod @splat
	Write-Verbose "Response received and parsed in $((Get-Date) - $timestamp)"
	
	$results
}

function Invoke-CherwellRequestFromURL {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipeline = $true,
				   ValueFromPipelineByPropertyName = $true)]
		[string[]]$Url,
		
		[Hashtable]$Headers = @{},
		
		[hashtable]$Body = $null,
		
		[AllowNull()]
		[int32]$Timeout,
		
		[ValidateSet('GET', 'PATCH', 'PUT', 'POST', 'DELETE', IgnoreCase = $true)]
		[string]$Method = 'GET',
		
		[ValidateSet('application/json', 'application/x-www-form-urlencoded', IgnoreCase = $true)]
		[string]$ContentType = "application/json"
	)
	
	BEGIN {	}
	
	PROCESS {
		foreach ($u in $Url) {
			Write-Verbose "Invoking request for $u"
			InvokeCherwellRequest -URI ([System.UriBuilder]::new($u)) -Headers $Headers -Body $Body -Timeout $Timeout -Method $Method -ContentType $ContentType
		}
	}
	
	END { }
}


function CompareValue {
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[string]$Value1,
		
		[Parameter(Mandatory = $true,
				   Position = 1)]
		[string]$Operator,
		
		[Parameter(Mandatory = $true,
				   Position = 2)]
		[string]$Value2
	)
	
	#Write-Verbose "COMPARE: $Value1 $Operator $Value2"
	
	switch ($Operator) {
		'Match' {
			return $Value1 -match $Value2
		}
		
		'Like' {
			return $Value1 -like $Value2
		}
		
		'NotLike' {
			return $Value1 -notlike $Value2
		}
		
		'Equal' {
			return $Value1 -eq $Value2
		}
		
		'NotEqual' {
			return $Value1 -ne $Value2
		}
		
		'StartsWith' {
			return $Value1 -like "$($Value2.TrimStart('*').TrimEnd('*'))*"
		}
		
		default {
			throw "Operator '$Operator' not supported."
		}
	}
}

function VerifyIDLength {
<#
	.SYNOPSIS
		Verifies the length of a BO/Rec ID
	
	.DESCRIPTION
		A detailed description of the VerifyIDLength function.
	
	.PARAMETER Id
		The ID to verify
	
	.EXAMPLE
		PS C:\> VerifyIDLength -Id 'Value1'
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	[OutputType([boolean])]
	param
	(
		[Parameter(Mandatory = $true,
				   ValueFromPipeline = $true,
				   Position = 0)]
		[string[]]$Id
	)
	
	Begin {
		# All Cherwell BO and Rec IDs are 42 characters.
		$validLength = 42
	}
	
	Process {
		foreach ($str in $id) {
			$validId = $str.Length -eq $validLength
			Write-Verbose "ID is valid: $validId"
			$validId
		}
	}
	
	End {
		
	}
}


function ProcessWebRequestException {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[System.Management.Automation.ErrorRecord]$Error
	)
	
	if ($Error.Exception.Response) {
		[System.Net.HttpWebResponse]$response = $Error.Exception.Response
		
		if ($Error.ErrorDetails.Message -match "The Business Object is not configured for searching") {
			return "The Business Object is not configured for searching"
		}
		
	}
}






