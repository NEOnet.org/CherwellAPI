﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2018 v5.5.148
	 Created on:   	3/14/2018 3:12 PM
	 Created by:   	 Ben Claussen
	 Organization: 	NEOnet
	 Filename:     	Core.ps1
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>

function Get-CherwellStoredValues {
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[ValidateSet('Global', 'User', 'Team', 'Role', 'Persona', IgnoreCase = $true)]
		[string]$Scope,
		
		[string]$ScopeOwnerId,
		
		[string]$FolderId,
		
		[switch]$IncludeLinks
	)
	
	$Segments = [System.Collections.ArrayList]@('storedvalues')
	
	if ($Scope) {
		$null = $Segments.AddRange(@('scope', $scope))
		
		if ($ScopeOwnerId) {
			if (-not (VerifyIDLength -Id $ScopeOwnerId)) {
				throw "ScopeOwnerId is not a valid length"
			}
			
			$null = $Segments.AddRange(@('scopeowner', $ScopeOwnerId))
			
			if ($FolderId) {
				if (-not (VerifyIDLength -Id $FolderId)) {
					throw "FolderId is not a valid length"
				}
				
				$null = $Segments.AddRange(@('folder', $FolderId))
			}
		}
	}
	
	if ($IncludeLinks) {
		$uriParameters = @{
			"Links" = $IncludeLinks
		}
	}
	
	$uri = BuildNewURI -Segments $Segments -Parameters $uriParameters
	
	$searchItems = InvokeCherwellRequest -URI $uri
	
#	if ($searchItems) {
#		$searchItems.supportedAssociations = $searchItems.supportedAssociations | Sort-Object -Property 'busObName' -Unique
#		$searchItems
#	}
}