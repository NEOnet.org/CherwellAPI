﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2017 v5.4.138
	 Created on:   	5/1/2017 11:44 AM
	 Created by:   	 Ben Claussen
	 Organization: 	NEOnet
	 Filename:     	Setup.ps1
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>

function SetupCherwellConfigVariable {
	[CmdletBinding()]
	param ()
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	Write-Verbose "Checking for CherwellConfig hashtable"
	if (-not ($script:CherwellConfig)) {
		Write-Verbose "Creating CherwellConfig hashtable"
		$script:CherwellConfig = [hashtable]@{
			'Connected' = $false
			'CachedData' = [hashtable]@{
				'BusinessObjects' = [hashtable]@{
					'Summaries' = $null
				}
				
				'SearchItems' = [hashtable]@{
					'SupportedAssociations' = [hashtable]@{
						
					}
					'SearchItems' = $null
				}
			}
		}
	}
	
	Write-Verbose "CherwellConfig hashtable already exists"
}

function GetCherwellConfigVariable {
	[CmdletBinding()]
	param
	(
		[switch]$Create
	)
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	Write-Verbose "Checking for CherwellConfig hashtable"
	if (-not $script:CherwellConfig) {
		Write-Verbose "CherwellConfig hashtable does not exist..."
		if (-not $Create) {
			throw "CherwellConfigVariable does not exist!"
		} else {
			SetupCherwellConfigVariable
		}
	} else {
		Write-Verbose "CherwellConfig hashtable exists!"
		return $script:CherwellConfig
	}
}

#region Caching functions - Unused

function InstantiateCherwellCache {
<#
	.SYNOPSIS
		Creates the necessary cached data in the CherwellConfig variable
	
	.DESCRIPTION
		A detailed description of the InstantiateCherwellCache function.
	
	.PARAMETER GetBusinessObjectSummaries
		A description of the GetBusinessObjectSummaries parameter.
	
	.PARAMETER BusinessObjectSummaries
		A description of the BusinessObjectSummaries parameter.
	
	.PARAMETER GetSearchItems
		A description of the GetSearchItems parameter.
	
	.PARAMETER SearchItems
		A description of the SearchItems parameter.
	
	.PARAMETER GetBusinessObjectSummaries
		A description of the GetBusinessObjectSummaries parameter.
	
	.PARAMETER GetSearchItems
		A description of the GetSearchItems parameter.
	
	.EXAMPLE
		PS C:\> InstantiateCherwellCache
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	param
	(
		[switch]$GetBusinessObjectSummaries,
		
		[object[]]$BusinessObjectSummaries,
		
		[switch]$GetSearchItems,
		
		[object[]]$SearchItems
	)
	
	if ($GetBusinessObjectSummaries) {
		if ($BusinessObjectSummaries) {
			Write-Verbose "Caching BusinessObjectSummaries from provided object"
			$script:CherwellConfig['CachedData']['BusinessObjects']['Summaries'] = $BusinessObjectSummaries
		} else {
			Write-Verbose "Caching BusinessObjectSummaries from API calls"
			# Get the object summaries from the API and stash it in the config
		}
	}
	
	if ($GetSearchItems) {
		
	}
}

function GetCherwellCache {
	[CmdletBinding(DefaultParameterSetName = 'BusinessObjectSummaries')]
	param
	(
		[Parameter(ParameterSetName = 'BusinessObjectSummaries',
				   Mandatory = $true)]
		[switch]$BusinessObjectSummaries,
		
		[Parameter(ParameterSetName = 'SearchItems',
				   Mandatory = $true)]
		[switch]$SearchItems
	)
	
	switch ($PSCmdlet.ParameterSetName) {
		'BusinessObjectSummaries' {
			return $script:CherwellConfig['CachedData']['BusinessObjects']['Summaries']
		}
		
		'SearchItems' {
			return $script:CherwellConfig['CachedData']['SearchItems']['SearchItems']
		}
		
		default {
			#<code>
		}
	}
}

#endregion Caching functions - Unused

function Set-CherwellHostName {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true)]
		[ValidateNotNullOrEmpty()]
		[string]$Hostname
	)
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	$script:CherwellConfig.Hostname = $Hostname.Trim()
	$script:CherwellConfig.Hostname
}

function Get-CherwellHostname {
	[CmdletBinding()]
	[OutputType([string])]
	param ()
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	if ($script:CherwellConfig.Hostname -eq $null) {
		throw "Cherwell Hostname is not set! You may set it with Set-CherwellHostname -Hostname 'hostname.domain.tld'"
	}
	
	$script:CherwellConfig.Hostname
}

function Set-CherwellCredentials {
	[CmdletBinding(DefaultParameterSetName = 'CredsObject')]
	[OutputType([pscredential], ParameterSetName = 'CredsObject')]
	[OutputType([pscredential], ParameterSetName = 'UserPass')]
	param
	(
		[Parameter(ParameterSetName = 'CredsObject',
				   Mandatory = $true)]
		[pscredential]$Credentials,
		
		[Parameter(ParameterSetName = 'UserPass',
				   Mandatory = $true)]
		[string]$Username,
		
		[Parameter(ParameterSetName = 'UserPass',
				   Mandatory = $true)]
		[string]$Password,
		
		[ValidateSet('Cherwell', 'LDAP', IgnoreCase = $true)]
		[string]$AuthenticationSource = 'LDAP'
	)
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	switch ($PsCmdlet.ParameterSetName) {
		'CredsObject' {
			$script:CherwellConfig.Credentials = $Credentials
			break
		}
		'UserPass' {
			$securePW = ConvertTo-SecureString $Password -AsPlainText -Force
			$script:CherwellConfig.Credentials = [System.Management.Automation.PSCredential]::new($Username, $securePW)
			break
		}
	}
	
	Set-CherwellAuthenticationSource -AuthenticationSource $AuthenticationSource
	
	$script:CherwellConfig.Credentials
}

function Get-CherwellCredentials {
	[CmdletBinding()]
	[OutputType([pscredential])]
	param ()
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	if (-not $script:CherwellConfig.Credentials) {
		throw "Cherwell Credentials not set! You may set with Set-CherwellCredentials"
	}
	
	$script:CherwellConfig.Credentials
}

function Set-CherwellAuthenticationSource {
	[CmdletBinding()]
	param (
		[ValidateSet('Internal', 'LDAP', IgnoreCase = $true)]
		[string]$AuthenticationSource = 'LDAP'
	)
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	$script:CherwellConfig.AuthenticationSource = $AuthenticationSource
	
	$script:CherwellConfig.AuthenticationSource
}

function Get-CherwellAuthenticationSource {
	[CmdletBinding()]
	[OutputType([string])]
	param ()
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	if (-not $script:CherwellConfig.AuthenticationSource) {
		throw "Cherwell Authentication Source not set! You may set with Set-CherwellAuthenticationSource"
	}
	
	$script:CherwellConfig.AuthenticationSource
}

function Set-CherwellAPIKey {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true)]
		[string]$APIKey
	)
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	$script:CherwellConfig.APIKey = $APIKey.Trim()
	$script:CherwellConfig.APIKey
}

function Get-CherwellAPIKey {
	[CmdletBinding()]
	[OutputType([string])]
	param ()
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	if ($script:CherwellConfig.APIKey -eq $null) {
		throw "Cherwell API Key is not set! You may set it with Set-CherwellAPIKey -APIKey 'key' or Connect-CherwellAPI"
	}
	
	$script:CherwellConfig.APIKey
}

function SetCherwellConfigToken {
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param
	(
		[Parameter(Mandatory = $true)]
		[pscustomobject]$Token
	)
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	<# Typical token response
		{
		  "access_token": "REALLYLONGSTRING",
		  "token_type": "bearer",
		  "expires_in": 1199,
		  "refresh_token": "string",
		  "as:client_id": "client-id",
		  "username": "username",
		  ".issued": "Mon, 01 May 2017 15:33:19 GMT",
		  ".expires": "Mon, 01 May 2017 15:53:19 GMT"
		}
	#>
	
	# Convert some values to better types for easier manipulation
	Write-Verbose "Setting and converting token values to PS types"
	$script:CherwellConfig.Token = [pscustomobject]@{
		'AccessToken' = $Token.access_token
		'TokenType' = $Token.token_type
		'ExpiresIn' = $Token.expires_in
		'RefreshToken' = $Token.refresh_token
		'AsClientId' = $Token.'as:client_id'
		'Username' = $Token.Username
		'Issued' = [datetime]$Token.'.issued'
		'Expires' = [datetime]$Token.'.expires'		
	}
}

function GetCherwellConfigToken {
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param ()
	
	if (-not $script:CherwellConfig.Token) {
		throw "Cherwell Token not set! You must connect to a Cherwell API to set the token!"
	}
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	$script:CherwellConfig.Token
}

function Set-CherwellAutoRefreshTokenSetting {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[boolean]$Enabled
	)
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	$script:CherwellConfig.AutoRefreshToken = $Enabled
}

function Get-CherwellAutoRefreshTokenSetting {
	[CmdletBinding()]
	[OutputType([boolean])]
	param ()
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	if ($script:CherwellConfig.AutoRefreshToken -eq $null) {
		throw "Cherwell AutoRefreshToken is not set! You may set it with Set-CherwellAutoRefreshToken -Enabled [`$true|`$false]"
	}
	
	$script:CherwellConfig.AutoRefreshToken
}

function Set-CherwellAPITimeout {
<#
	.SYNOPSIS
		Sets the timeout in seconds for the API calls
	
	.DESCRIPTION
		Sets the amount of time in seconds for which the API request will wait before timing out and returning an error
	
	.PARAMETER Seconds
		An integer representing seconds
	
	.EXAMPLE
		PS C:\> Set-CherwellAPITimeout -Seconds $value1
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[ValidateRange(1, 60)]
		[int32]$Seconds
	)
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	$script:CherwellConfig.Timeout = $Seconds
}

function Get-CherwellAPITimeout {
	[CmdletBinding()]
	[OutputType([int32])]
	param ()
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	if ($script:CherwellConfig.Timeout -eq $null) {
		throw "Cherwell API timeout is not set! You may set it with Set-CherwellAPITimeout -Seconds [int32]"
	}
	
	$script:CherwellConfig.Timeout
}

function Get-CherwellToken {
	[CmdletBinding()]
	[OutputType([pscustomobject])]
	param ()
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	$uri = BuildNewURI -Hostname (Get-CherwellHostname) -TokenRequest -Parameters @{
		'api_key' = (Get-CherwellAPIKey)
		'auth_mode' = (Get-CherwellAuthenticationSource)
	}
	
	$header = @{
		"Accept" = "application/json"
		"Content-Type" = "application/x-www-form-urlencoded"
	}
	
	# The body contains the client ID and the credentials for the call
	$body = @{
		"grant_type" = "password"
		"client_id" = (Get-CherwellAPIKey)
		"username" = (Get-CherwellCredentials).UserName
		"password" = (Get-CherwellCredentials).GetNetworkCredential().Password
	}
	
	return InvokeCherwellRequest -URI $uri -Headers $header -Body $body -Method POST -TokenRequest -ContentType application/x-www-form-urlencoded
}

function Connect-CherwellAPI {
<#
	.SYNOPSIS
		Connect to a Cherwell API
	
	.DESCRIPTION
		Connect a session to a Cherwell API by obtaining a token and optionally pre-caching some data.
	
	.PARAMETER Hostname
		The DNS name of the Cherwell server hosting the API application
	
	.PARAMETER Credentials
		Credentials to access the Cherwell API. If not provided as a parameter, you will be prompted.
	
	.PARAMETER AuthenticationSource
		The source for which the credentials should be checked against. Defaults to LDAP
	
	.PARAMETER APIKey
		A valid API key created by a Cherwell Administrator. This key is not necessarily associated with a 
		particular user.
	
	.PARAMETER AutoRefreshToken
		When enabled, if the token has expired since first connecting, automatically submit the request to refresh the 
		token for another period of time. Default is enabled.
	
	.PARAMETER Timeout
		The timeout in seconds for the HTTP call to the Cherwell server. Sometimes token requests take a little longer 
		to generate. Defaults to 5 seconds.
	
	.PARAMETER InstantiateCache
		This will perform lookups for [BusinessObjects, SearchItems] and cache the information to the local session. If
		more cmdlets run which would normally contact the server, the local cache will be used instead. Aforementioned 
		cmdlets can be forced to reference the server by the -Force parameter.
	
	.EXAMPLE
		PS C:\> Connect-CherwellAPI -Hostname 'cherwell.mydomain.com' -APIKey 'some-guid-here'
	
	.NOTES
		Additional information about the function.
#>
	
	[CmdletBinding()]
	[OutputType([boolean])]
	param
	(
		[Parameter(Mandatory = $true)]
		[string]$Hostname,
		
		[pscredential]$Credentials,
		
		[ValidateSet('Internal', 'LDAP', IgnoreCase = $true)]
		[string]$AuthenticationSource = 'LDAP',
		
		[Parameter(Mandatory = $true)]
		[string]$APIKey,
		
		[boolean]$AutoRefreshToken = $true,
		
		[int32]$Timeout = 10,
		
		[boolean]$InstantiateCache = $true
	)
	
	Write-Debug "$($PSCmdlet.MyInvocation.InvocationName) called"
	
	Write-Progress -Id 1 -Activity "Connecting to CherwellAPI at $Hostname" -PercentComplete 25 -CurrentOperation "Configuring environment"
	
	SetupCherwellConfigVariable
	
	if (-not $Credentials) {
		Write-Verbose "Credentials not provided..."
		
		if ($AuthenticationSource -eq 'Internal') {
			$credMessage = 'Enter INTERNAL credentials for Cherwell in cherwell\username format.'
		} else {
			$credMessage = "Enter $($AuthenticationSource.ToUpper()) credentials for Cherwell (domain\username)"
		}
		
		Write-Progress -Id 1 -Activity "Connecting to CherwellAPI at $Hostname" -PercentComplete 25 -CurrentOperation "Obtaining credentials"
		
		if (-not ($Credentials = Get-Credential -Message $credMessage)) {
			throw "Credentials are necessary to connect to a Cherwell API service."
		}
	}
	
	Write-Progress -Id 1 -Activity "Connecting to CherwellAPI at $Hostname" -PercentComplete 50
	
	$null = Set-CherwellHostName -Hostname $Hostname
	$null = Set-CherwellAuthenticationSource -AuthenticationSource $AuthenticationSource
	$null = Set-CherwellCredentials -Credentials $Credentials
	$null = Set-CherwellAPIKey -APIKey $APIKey
	$null = Set-CherwellAPITimeout -Seconds $Timeout
	$null = Set-CherwellAutoRefreshTokenSetting -Enabled $AutoRefreshToken
	
	# Try to obtain a token from the service
	try {
		Write-Progress -Id 1 -Activity "Connecting to CherwellAPI at $Hostname" -PercentComplete 75 -CurrentOperation "Obtaining token from Cherwell"
		$token = Get-CherwellToken -ErrorAction Stop
		SetCherwellConfigToken -Token $token
		
		Write-Progress -Id 1 -Activity "Connecting to CherwellAPI at $Hostname" -Completed
		
		$script:CherwellConfig.Connected = $true
		return $true
	} catch {
		if ($_.Response) {
			try {
				$result = $_.Exception.Response.GetResponseStream()
				$reader = New-Object System.IO.StreamReader($result)
				$reader.BaseStream.Position = 0
				$reader.DiscardBufferedData()
				$responseBody = $reader.ReadToEnd() | ConvertFrom-Json
				
				switch ($responseBody.error) {
					'invalid_grant' {
						throw "Invalid username/password provided"
						break
					}
					
					'invalid_client_id' {
						throw $responseBody.error_description
						break
					}
					
					default {
						throw $_
					}
				}
			} catch {
				throw $_
			}
		} else {
			throw $_
		}
	}
}







