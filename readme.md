## CherwellAPI
This is a Powershell Module for working with Cherwell's REST API.

---

In order to install it, copy the `dist` folder to a Powershell Module folder and rename it to `CherwellAPI`.

---

### This is a work in progress! Not all help content has been updated appropriately!

### For debugging, all commands are exported currently, including the commands which should NOT be exported.

### All user-facing commands are in the usual "`verb-noun`" format